# **网站首页开发平台** #

本网站后台基于Jfinal开发，数据库为Postgresql或Mysql。
演示地址：http://websitejfinal.duapp.com/
测试账号: test/123456

*附件有部署好的postgre、tomcat以及项目相关lib包。*
# 一、平台部署说明 #
------------------------
## **1、下载website_jfinal项目** ##
#### 1）数据库配置文件：/website_jfinal/src/conf/db.properties ####

## **2、数据库安装** ##
#### 2）安装postgresql/mysql数据库。 ####
#### 3）创建用户website/website,创建数据库website，所属用户website ####
#### 4）postgresql导入数据库文件website.backup####
#### 导入命令： ####
```java
pg_restore.exe --host 127.0.0.1 --port 54321 --username "website" --dbname "website" --no-password  --schema public --verbose "D:\website.backup"
```

#### mysql运行website_mysql.sql文件 ####

## **3、Tomcat安装** ##
#### 6）安装Tomcat中间件 ####
#### 7）部署website_jfinal ####
###### *可以将data/website.xml放在tomcat的conf\Catalina\localhost目录下* ######

# 备注说明 #
###### *1）效果截图、lib包、1.0源码地址：http://pan.baidu.com/s/1kTgD4Sf
###### *2）由于本人很少接触网站开发，望大家多提意见。* ######
###### *3）此系统部分测试数据借鉴某网站首页，尽情见谅。* ######