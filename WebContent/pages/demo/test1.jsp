<%@page import="com.flyfox.modules.column.TbColumn"%>
<%@page import="com.flyfox.util.StrUtils"%>
<%@page import="com.flyfox.modules.column.ColumnSvc"%>
<%@page import="com.flyfox.modules.dict.DictCache"%>
<%@page import="com.flyfox.util.NumberUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.flyfox.util.DateUtils"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>XX企业集团网站</title>
<link href="./test1/css.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="jquery-1.8.2.min.js"></script>
	<%
		String para_id = request.getParameter("id");
		TbColumn article = ColumnSvc.getColumn(NumberUtils.parseInt(para_id, 8));
		TbColumn parent_article = ColumnSvc.getColumn(article.getInt("parent_id"));
	%>
	<script type="text/javascript">	
	function show1(pid,title) {
		$("table[class^='daniu']").hide();
		$("table.daniu_" + pid).show();
		$("#title2").html(title);
	}

	$(function($) {
		$("table[class^='daniu']").hide();
		$("table.daniu_<%=article.getInt("parent_id")%>").show();
		$("#title2").html('<%=article.getInt("level")==2?article.getStr("title"):parent_article.getStr("title") %>');
	});
	</script>
</head>

<body>
	<table width="982" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td bgcolor="#10477F" height="4"></td>
		</tr>
		<tr>
			<td><img src="./test1/home_04.gif" width="247" height="78"
				alt=""><img src="./test1/home_05.gif" width="735" height="78"
				alt="">
			</td>
		</tr>
		<tr>
			<td><table width="100%" border="0" cellpadding="0"
					cellspacing="0" bgcolor="#E8E8E8">
					<tr>
						<td width="247" align="center" valign="bottom"><table
								width="222" height="33"
								style="background: url(test1/home_17.gif) top left no-repeat"
								border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center" class="white">当前时间：<%=DateUtils.format(DateUtils.getCurrentDate(), "yyyy年MM月dd日")%></td>
								</tr>
							</table>
						</td>
						<td>
						<%
								int type,id,width;
								String title;
								List<TbColumn> list1 = ColumnSvc.getListByParentId(1);
								for (TbColumn column : list1) {
									type = NumberUtils.parseInt(DictCache.getCode(column.getInt("type")));
									id = column.getInt("id");
									title = column.getStr("title");
									
									if(id==8) width = 76;
									else if(id==19) width = 87;
									else width = 95;
									
							%><% if(type==1){ %><a href="javascript:show1(<%=id %>,'<%=column.getStr("title") %>')"><% } else if(type==2){ %><a href="test1.jsp?id=<%=id %>"><% } else if(type==3){ %><a href="../../article/show/<%=id %>" target="_blank "><%} %><img src="../../download/image_url/<%=column.getStr("image_url") %>" 
							alt="" width="<%=width %>" height="36" border="0"></a><%
								}
							%><img src="./test1/home_11.gif" alt="" width="94" height="36"
								border="0">
						</td>
					</tr>
					<tr>
						<td height="22" align="center"><img src="./test1/icon11.gif"
							width="20" height="20" align="absmiddle">访问人数：<%=(int)(Math.random()*8000) %></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table width="982" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td width="12" style="background: #F5F5F5"></td>
			<td width="222" height="476" valign="top" style=""><table
					width="213" align="center" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td height="8"></td>
					</tr>
					<tr>
						<td><table width="95%" border="0" align="center"
								cellpadding="0" cellspacing="0" bgcolor="#B60000">
								<tr>
									<td width="18"><img src="./test1/home_28.gif" width="18"
										height="23" alt="">
									</td>
									<td class="white"><strong id="title2"></strong>
									</td>
									<td width="12"><img src="./test1/home_30.gif" width="12"
										height="23" alt="">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
					<%
					for (TbColumn column1 : list1) {
					%>
					<table  class="daniu_<%=column1.getInt("id")%>" width="200" align="center" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<td></td>
						<td height="15"></td>
					</tr>
						<%
							List<TbColumn> list2 = ColumnSvc.getListByParentId(column1.getInt("id"));
								for (TbColumn column2 : list2) {
									type = NumberUtils.parseInt(DictCache.getCode(column2.getInt("type")));
									id = column2.getInt("id");
									title = column2.getStr("title");
						%>
						<tr>
						<td width="22" class="col1"><img src="./test1/home_43.gif"
							alt="" width="11" height="11" hspace="5" vspace="5"
							align="absmiddle">
						</td>
						<td class="col">
							<% if(type==1){ %>
								<a href="javascript:show2(<%=id %>)"><%=title %></a>
							<% } else if(type==2){ %>
								<a href="test1.jsp?id=<%=id %>"><%=title %></a>
							<% } else if(type==3){ %>
								<a href="../../article/show/<%=id %>" target="_blank "><%=title %></a>
							<%} %>
							</td>
						</tr>
						<%
							}
						%>
					</table>
					<%
						}
					%>
			</td>
			<td width="12" style="background: #F5F5F5"></td>
			<td valign="top"><table width="100%" height="7" border="0"
					cellpadding="0" cellspacing="0">
					<tr>
						<td></td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="15"></td>
						<td height="425" valign="top"><table width="100%" border="0"
								cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
								<tr>
									<td width="9" height="8"><img src="./test1/zs.gif"
										width="9" height="8">
									</td>
									<td></td>
									<td width="8"><img src="./test1/ys.gif" width="9"
										height="8">
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="0"
											bgcolor="#104780">
											<tr>
												<td width="15"><img src="./test1/home_32.gif"
													width="15" height="23" alt="">
												</td>
												<td class="white"><strong><%=article.getStr("title")%></strong>
												</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0"
											cellpadding="15">
											<tr>
												<td valign="top" height="350"><%=StrUtils.nvl(article.getStr("content"), "暂无")%>
												</td>
											</tr>
										</table></td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td height="8"><img src="./test1/zx.gif" width="9"
										height="8">
									</td>
									<td></td>
									<td><img src="./test1/yx.gif" width="9" height="8">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" height="7" border="0" cellpadding="0"
					cellspacing="0">
					<tr>
						<td></td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0">
					<tr>
						<td height="38" align="right" bgcolor="#104780" class="white"
							style="line-height: 120%;"><img src="./test1/home_71.jpg"
							alt="" width="365" height="5" vspace="5"><br>
							电话：010-66666666 传真：010-88888888 E-mail：niub@abcd.com <a
							href="javascript:void(0);" target="_blank" class="nav2">京ICP备68686868号</a>
							<a href="javascript:void(0);" target="_blank" class="nav2">axaxax</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


</body>
</html>