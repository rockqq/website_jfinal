<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/static/component/include/head.jsp"%>

<script type="text/javascript">
paginator = function(page){
	oper_list();
};

function oper_list(){
	form1.action = 'state/list';
	form1.submit();
}

function oper_view(pid){
	Iframe('state/view/'+pid,300,240,'任务状态查看',false,false,false,EmptyFunc);
}

function oper_add(){
	Iframe('state/add',350,250,'任务状态添加');
}

function oper_edit(pid){
	Iframe('state/edit/'+pid,350,250,'任务状态修改');
}

function oper_delete(pid){
	Confirm("确认要删除该任务状态信息？",function(){
		form1.action = 'state/delete/'+pid;
		form1.submit();
	});
}

$(function(){
	//显示Menu索引
	$(".menu_index").show();
});
</script>
</head>
<body>
	<form name="form1" action="" method="post">
		<div id="tools">
			<div class="tools_l">
				<span class="new_page">固定内容定义</span>
			</div>
			<div class="tools_r">
				<%@include file="/static/component/include/menu.jsp"%> 
			</div>
		</div>
		<div class="tableSearch">
			<fieldset>
				<table>
					<tr>
						<td>
							状态编码
							<input name="stateNum" type="text" value="${attr.stateNum }">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="btn1" value="查 询" onclick="oper_list();" name="search" />
							<input type="button" class="btn1" value="新 增" onclick="oper_add();" />
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
		<!-- 数据列表 -->
		<div class="tableList">
			<table>
				<thead>
					<tr>
						<th>序号</th>
						<th>设备编码</th>
						<th>类型</th>
						<th>状态</th>
						<th>操作</th>
					</tr>
				</thead>
				<c:forEach items="${requestScope.list}" var="item" varStatus="row">
					<tbody>
						<tr>
							<td width="50">${row.count }</td>
							<td>${item.stateNum}</td>
							<td>${dictMap[item.stateType].detailName}</td>
							<td>${item.state}</td>
							<td align="center">
								<a href="javascript:void(0);" class="view" onclick="oper_view(${item.stateId});">查看</a> 
								<a href="javascript:void(0);" class="edit" onclick="oper_edit(${item.stateId});">修改</a> 
								<a href="javascript:void(0);" class="delete" onclick="oper_delete(${item.stateId});">删除</a>
							</td>
						</tr>
					</tbody>
				</c:forEach>
			</table>
		<%@include file="/static/component/include/paginator.jsp"%>
		</div>
	</form>
</body>

</html>
