<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/static/component/include/head.jsp"%>
<%@include file="/static/component/include/my97.jsp"%>
<script type="text/javascript">
function oper_save(){
	if(validForm()){
		form1.action="state/save";
		form1.submit();
	}
}
</script>
</head>
<body>
	<form name="form1" action="" method="post">
		<input type="hidden" name="stateId" value="${item.stateId}" /> 
		<div id="tools">
			<span class="new_page">${item.stateId>0?'任务状态修改':'任务状态添加'}</span>
		</div>
		<!-- 数据列表 -->
		<div class="tableEdit" >
			<table>
				<tr>
					<td>状态编码</td>
					<td>
						<c:choose>
							<c:when test="${item.stateId >0 }">
								${item.stateNum}
								<input type="hidden" name="stateNum" value="${item.stateNum}" />
							</c:when>
							<c:otherwise>
								<input type="text" name="stateNum" value="${item.stateNum}" valid="vtext" validname="状态编码" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>类型</td>
					<td>
						<jsp:setProperty property="select" name="dict" value="${item.stateType }|stateType"/>
						<select name="stateType" style="width: 100px;">
							${dict.select}
						</select>
					</td>
				</tr>
				<tr>
					<td>状态</td>
					<td><input type="text" name="state" value="${item.state}" valid="vtext" validname="状态"  /></td>
				</tr>
			</table>
		</div>
		<div id="bottom">
			<input type="button" class="btn1" value="保 存" onclick="oper_save();" />
			<input type="button" class="btn1" value="关 闭" onclick="closeIframe();" />
		</div>
	</form>
</body>

</html>
