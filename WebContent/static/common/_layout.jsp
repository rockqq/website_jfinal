<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="jsp_layout.tld" prefix="layout"%>
<!DOCTYPE html>
<html>
<head>
<layout:block name="head"></layout:block>
</head>
<body>
	<layout:block name="body"></layout:block>
</body>
</html>